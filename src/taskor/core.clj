(ns taskor.core
  (:require [clojure.java.io :as io]
            [clojure.pprint :as pp])
  (:gen-class))

(defn analyse-tasks
  "Opens the file F and searches line by line for occurrences of TODO or FIXME."
  [f]
  (with-open [rdr (clojure.java.io/reader f)]
    (let [lines (doall (map-indexed vector (line-seq rdr)))
          results (filter #(re-matches #".*(TODO|FIXME).*" (last %)) lines)]
      (if (not (empty? results))
        {:file (.getCanonicalPath (io/file f)) :matches results}
        {}))))

(defn process-files [f fun]
  (let [fil (io/file f)]
    (if (.isFile fil)
      (apply fun [f]))))

(defn list-files [dir]
  (->> dir
       file-seq
       (map #(process-files % analyse-tasks))
       flatten
       (filter (comp not empty?))))

(defn display-entries [res]
  (doseq [r res]
    (do (println (r :file))
        (doseq [m (r :matches)]
          (println (inc (first m)) ": " (last m))))
    (println)))

(defn -main [& args]
  (-> (clojure.java.io/file ".")
      list-files
      display-entries))
